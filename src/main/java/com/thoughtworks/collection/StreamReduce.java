package com.thoughtworks.collection;

import java.util.List;
import java.util.Optional;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
        return numbers.stream().reduce((odd, number) -> number % 2 == 1 ? number : odd).orElse(-1);
    }

    public String getLongest(List<String> words) {
        return words.stream().reduce((longestWord, word) -> word.length() > longestWord.length() ? word : longestWord).orElse("");
    }

    public int getTotalLength(List<String> words) {
        return Integer.parseInt(words.stream().reduce("0", (totalLength, word) -> {
            int totalLengthNum = Integer.parseInt(totalLength);
            totalLengthNum += word.length();
            return String.valueOf(totalLengthNum);
        }));
    }
}
